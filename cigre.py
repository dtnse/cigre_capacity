import numpy as np
import logging
from scipy.constants import convert_temperature

logging_FMT = "%(asctime)s - %(levelname)s - %(funcName)s - %(message)s"
logging.basicConfig(level=logging.INFO, format=logging_FMT)

# define constants
absorptivity = 0.5
g = 9.81
emissivity = 0.5
stefb = 0.0000000567
powerline_height = 10

def compute_angle(wind_speed):
    """function description"""

    stdev_to_avg = 0.78

    return np.where(wind_speed >= 1, stdev_to_avg * (31.641 * wind_speed ** (-0.815)), stdev_to_avg * 31.641)

def compute_cooling_capacity(tt, ts, wind_speed, radiation, angle, diameter_wire):
    """function description"""

    logging.info('Computing Solar Heating')
    solar_heating = compute_solar_heating(radiation, diameter_wire)
    logging.info('Computing Cooling Air')
    air_cooling = compute_cooling_air(
        tt, ts, wind_speed, np.radians(angle), diameter_wire
    )
    logging.info('Computing Radiative Cooling')
    radiative_cooling = compute_radiative_cooling(ts, tt, diameter_wire)
    return -solar_heating + air_cooling + radiative_cooling


def ICapRelMC2(
    tt,
    ts,
    wind_speed,
    radiation,
    altitude,
    angle,
    is_angle_fixed,
    diameter_wire,
    max_cooling_capacity,
):

    if not is_angle_fixed:
        angle += compute_angle(wind_speed)

    if angle > 90:
        angle = np.abs(180 - angle)

    cooling_capacity = compute_cooling_capacity(
        tt, ts, wind_speed, radiation, angle, diameter_wire
    )
    icaprel = np.sqrt(cooling_capacity / max_cooling_capacity)

    return icaprel


def compute_solar_heating(radiation, diameter_wire):
    """function description"""

    return absorptivity * radiation * diameter_wire


def compute_cooling_air(tt, ts, wind_speed, angle, diameter_wire):
    """function description"""

    logging.info('Computing T_film')
    t_film = np.mean([tt,np.full_like(tt, ts) ], axis=0, dtype=np.float64)
    logging.info('Computing Grashof')
    grashof = compute_grashof(diameter_wire, ts, tt, t_film)
    logging.info('Computing Prantl')
    prantl = compute_prantl(t_film)
    logging.info('Computing Rayleigh')
    rayleigh = compute_rayleigh(grashof, prantl)
    logging.info('Computing nuNat')
    nuNat = compute_nuNat(rayleigh)
    logging.info('Computing Reynolds')
    reynolds = compute_reynolds(
        wind_speed,
        compute_rhoRel(powerline_height),
        diameter_wire,
        compute_kinVis(t_film),
    )
    logging.info('Computing Nusselt')
    nusselt = compute_nusselt(reynolds)
    logging.info('Computing nuConv')
    nuConv = compute_nuConv(compute_wind_factor_angle(angle), nusselt)
    logging.info('Computing nuWeinigWind')
    nuWeinigWind = compute_nuWeinigWind(nusselt)
    return (
        np.pi
        * compute_air_thermal_conductivity(t_film)
        * (ts - tt)
        * compute_nuEffec(wind_speed, nuConv, nuWeinigWind, nuNat)
    )


def compute_radiative_cooling(t1, t2, diameter_wire):
    """function description"""

    t1 = convert_temperature(t1, 'Celsius', 'Kelvin')
    t2 = convert_temperature(t2, 'Celsius', 'Kelvin')

    return np.pi * diameter_wire * emissivity * stefb * (t1 ** 4 - t2 ** 4)


def compute_reynolds(wind_speed, RHO, diameter_wire, visc):
    """Computation of Reynolds number"""

    return RHO * wind_speed * diameter_wire / visc


def compute_rhoRel(altitude):
    """function description"""

    return np.exp(-0.000116 * altitude)


def compute_nusselt(reijn):
    """Computation of Nusselt number"""

    condition = reijn < 2650
    if_true = 0.641 * reijn ** 0.471
    if_false = 0.0481 * reijn ** 0.8

    return np.where(condition, if_true, if_false)


def compute_nuConv(hoekFactorWind, nusselt):
    """function description"""

    return hoekFactorWind * nusselt


def compute_nuWeinigWind(nusselt):
    """function description"""

    return nusselt * 0.55


def compute_wind_factor_angle(angleRad):
    """function description"""

    a1 = 0.42
    condition = np.degrees(angleRad) > 24
    if_true = a1 + 0.58 * np.sin(angleRad) ** 0.9
    if_false = a1 + 0.68 * np.sin(angleRad) ** 1.08

    return np.where(condition, if_true, if_false)


def compute_air_thermal_conductivity(t_film):
    """function description"""

    return (2.42 / 100.0) + (7.2e-5) * t_film


def compute_nuEffec(wind_speed, nuConv, nuWeinigWind, nuNat):
    """function description"""

    condition1 = wind_speed < 0.5
    condition2 = np.logical_and((nuConv > nuWeinigWind),(nuConv > nuNat))
    condition3 = np.logical_and((nuWeinigWind > nuConv),(nuWeinigWind > nuNat))

    if_true3 = nuWeinigWind
    if_false3 = nuNat

    if_true2 = nuConv    
    if_false2 = np.where(condition3, if_true3, if_false3)

    if_true1 = np.where(condition2, if_true2, if_false2)
    if_false1 = nuConv

    return np.where(condition1, if_true1, if_false1)


def compute_kinVis(t_film):
    """function description"""

    return 1.32e-5 + (9.5e-8) * t_film


def compute_prantl(t_film):
    """Computation of Prandtl number"""

    return 0.715 - (2.5 / 10000) * t_film


def compute_grashof(diameter_wire, ts, tt, t_film):
    """Computation of Grashof number"""

    return (
        diameter_wire ** 3
        * (ts - tt)
        * g
        / ((t_film + 273) * compute_kinVis(t_film) ** 2)
    )


def compute_nuNat(rayleigh):
    """function description"""

    return np.where(rayleigh < 1000, 0.85 * rayleigh ** 0.188, 0.48 * rayleigh ** 0.25)



def compute_rayleigh(grashof, prantl):
    """function description"""

    return grashof * prantl


if __name__ == '__main__':

    tt = 10
    ts = 32
    wind_speed = 15
    radiation = 13
    altitude = powerline_height
    angle = 40
    is_angle_fixed = True
    diameter_wire = 4
    max_cooling_capacity = 3

    result = ICapRelMC2(tt, ts, wind_speed, radiation, altitude, angle, is_angle_fixed, diameter_wire, max_cooling_capacity)
    # print('ICapRelMC2', result)

    # tt - Air Temperature (C)
    # ts - Wire Temperature (C)
    # wind_speed - Wind speed (m s^-1)
    # radiation - Radiation (W m^-2)
    # altitude - height of the powerline
    # angle - angle between wind and powerline
    # is_angle_fixed - True if angle between wind and powerline is fixed (bool)
    # diameter_wire - Wire diameter
    # max_cooling_capacity - 

