# CIGRE capacity formula

Python implementation of the CIGRÉ capacity formula

Source documentation for capacity formula: https://e-cigre.org/publication/207-thermal-behaviour-of-overhead-conductors

Code diagram:

![](CIGRE.PY_diagram.png)

The main function to compute capacity is cigre.compute_cooling_capacity(). This function takes as input atmospheric data such as air temperature, wind speed and radiation.

Here's an example of usage:

```
import cigre
import xarray as xr

fn = "/path/to/example.nc"
ds = xr.open_dataset(fn)

tt = ds.masked_t2m.values - 273.15          # convert from K to C
ts = 80                                     # Reference value
u10 = ds.masked_u10.values
v10 = ds.masked_v10.values
wind_speed = np.hypot(u10, v10)
radiation = ds.masked_ssrd.values / 3600    # convert from J*m**-2 to W*m**-2

angle = 90
diameter_wire = 27.9

capacity = cigre.compute_cooling_capacity(
    tt, ts, wind_speed, radiation, angle, diameter_wire
)
```